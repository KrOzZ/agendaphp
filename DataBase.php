<?php

class DataBase
{
    private $conn;
    public static $WHERE_OR = 1;
    public static $WHERE_AND = 2;
    public $lastQuery = "";
    public function __construct()
    {

        $this->conn = new mysqli("localhost", "carlossant47", "SanchezZa45", "agenda");
        if($this->conn->connect_error)
        {
            echo $this->conn->connect_error;
            die();
        }
        $this->conn->set_charset("utf8");
    }

    public function setOtherConnection($host, $user, $pass, $db)
    {
        $this->conn->close();
        $this->conn = new mysqli($host, $user, $pass, $db);
        if($this->conn->connect_error)
        {
            echo $this->conn->connect_error;
            die();
        }
        $this->conn->set_charset("utf8");
    }

    /*
    private function clean()
    {
        $this->innerColumns = array();
        $this->whereColumn = array();
        $this->selectColumns = "";
        $this->likeColumns = array();
    }
    */

    private function getQuerySet($params)
    {
        $count = 0;
        $totalParams = count(array_keys($params));
        $query = "";
        foreach ($params as $key => $value) {
            if ($count == ($totalParams - 1))
                $query .= "$key = '$value'";
            else
                $query .= "$key = '$value',";
            $count++;
        }

        return $query;
    }

    public function insert($tableName, $params)
    {
        $data['affected_rows'] = 0;
        $data['insert_id'] = 0;
        $query = "INSERT INTO $tableName SET ";
        if (!is_array($params)) {
            return false;
        }
        $query .= $this->getQuerySet($params);
        if ($this->conn->query($query)) {
            $data['affected_rows'] = $this->conn->affected_rows;
            $data['insert_id'] = $this->conn->insert_id;
        }
        $this->lastQuery = $query;
        return (object)$data;
    }


    /**
     * @param $tableName string
     * @param $params array
     * @param $where string
     * @return bool|object
     */
    public function update($tableName, $params, $where)
    {
        $data['affected_rows'] = 0;
        $query = "UPDATE $tableName SET ";
        if (!is_array($params)) {
            return false;
        }
        $query .= $this->getQuerySet($params);
        $query .= " where $where";
        if ($this->conn->query($query)) {
            $data['affected_rows'] = $this->conn->affected_rows;
        }
        $this->lastQuery = $query;
        return (object)$data;
    }

    public function select($query, $isObject = false)
    {
        $counter = 0;
        $data = array();
        $result = $this->conn->query($query);
        if(!$result)
        {
            printf("Errormessage: %s\n", $this->conn->error);
            die();
        }


        while($row = $result->fetch_array(MYSQLI_ASSOC))
        {
            if($isObject)
                $data[] = (object) $row;
            else
                $data[] = $row;
        }
        return $data;
    }


}


