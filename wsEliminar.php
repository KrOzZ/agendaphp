<?php
/**
 * Created by PhpStorm.
 * User: CarlosSant47
 * Date: 3/27/2020
 * Time: 10:24 PM
 */
require 'Contacto.php';

if(!isset($_GET['_ID']))
{
	
    echo json_encode(["status" => false, "op"=> "eliminar"]);
    die();
}

$id = (int) $_GET['_ID'];
$contacto = new Contacto();
$result['status'] = $contacto->deleteContacto($id);
$result['op'] = "eliminar";
$result['id'] = (int) $_GET['_ID'];
echo json_encode($result);