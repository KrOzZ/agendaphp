<?php
require 'DataBase.php';
class Contacto
{
	private $db;
	public $lastQuery = "";

	public function __construct()
	{
		$this->db = new DataBase();
	}

	public function insertContacto($data) : bool
	{
	    $exito = false;
	    if(!is_object($data))
	        return $exito;
		$dataInsert["nombre"] = $data->nombre;
		$dataInsert["telephone1"] = $data->telefono1;
		$dataInsert["telephone2"] = $data->telefono2;
		$dataInsert["direccion"] = $data->direccion;
		$dataInsert["notas"] = $data->notas;
		$dataInsert["favorite"] = $data->favorite;
		$dataInsert["secure_id"] = $data->idMovil;
		if($this->db->insert("contactos", $dataInsert)->affected_rows >= 1)
        {
            $exito = true;
        }
        $this->lastQuery = $this->db->lastQuery;
        return $exito;

	}

    /**
     * @param $data object
     * @param $id int
     * @return bool
     */
    public function updateContacto($data, $id ) : bool
	{
        $exito = false;
        if(!is_object($data))
            return $exito;
        $dataInsert["nombre"] = $data->nombre;
		$dataInsert["telephone1"] = $data->telefono1;
		$dataInsert["telephone2"] = $data->telefono2;
		$dataInsert["direccion"] = $data->direccion;
		$dataInsert["notas"] = $data->notas;
		$dataInsert["favorite"] = $data->favorite;
		$dataInsert["secure_id"] = $data->idMovil;
        if($this->db->update("contactos", $dataInsert, "id = $id")->affected_rows >= 1)
        {
            $exito = true;
        }
        return $exito;
	}

	public function deleteContacto($id) : bool
	{
	    $exito = false;
	    $dataUpdate["status"] = 0;
	    if($this->db->update("contactos", $dataUpdate, "id = $id")->affected_rows >= 1)
	        $exito = true;
	    $this->lastQuery = $this->db->lastQuery;
	    return $exito;
	}

    /**
     * @param $android_secure_id string
     * @return array
     */
    public function consultContacto($android_secure_id) : array
	{
	    return $this->db->select("select id as _ID, nombre, telephone1 as telefono1, telephone2 as telefono2, direccion, notas, favorite, secure_id as idMovil from contactos where secure_id = '$android_secure_id' and status = 1  ");
	}
}



