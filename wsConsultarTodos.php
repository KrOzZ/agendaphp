<?php
/**
 * Created by PhpStorm.
 * User: CarlosSant47
 * Date: 3/27/2020
 * Time: 10:24 PM
 */
require 'Contacto.php';

if(!isset($_GET['idMovil']))
{
	
    echo json_encode(["status" => false, "op"=> "consultar"]);
    die();
}
$secure_id = (string) $_GET['idMovil'];
$contacto = new Contacto();
$result['contactos'] = $contacto->consultContacto($secure_id);
$result['op'] = 'consultar';
echo json_encode($result);
